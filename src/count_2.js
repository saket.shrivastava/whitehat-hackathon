import {Finger, FingerCurl, FingerDirection, GestureDescription} from 'fingerpose'; 

// Define Gesture Description
export const CountTwo = new GestureDescription('count_2'); 

// thumb:
// - not curled
// - vertical up (best) or diagonal up left / right
CountTwo.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0);
CountTwo.addDirection(Finger.Index, FingerDirection.VerticalUp, 0.9);
CountTwo.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 0.2);
CountTwo.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 0.2);

CountTwo.addCurl(Finger.Middle, FingerCurl.NoCurl, 1.0);
CountTwo.addDirection(Finger.Middle, FingerDirection.VerticalUp, 1.0);

CountTwo.addCurl(Finger.Thumb, FingerCurl.HalfCurl, 0.5);
CountTwo.addCurl(Finger.Thumb, FingerCurl.NoCurl, 0.5);
CountTwo.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1);
CountTwo.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1);

for(let finger of [Finger.Ring, Finger.Pinky]) {
    CountTwo.addCurl(finger, FingerCurl.FullCurl, 1.0);
    CountTwo.addDirection(finger, FingerDirection.VerticalDown, 0.8);
}

CountTwo.setWeight(Finger.Middle, 2);
  

