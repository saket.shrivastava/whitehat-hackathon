import React, { useRef, useState, useEffect } from "react";
import * as tf from "@tensorflow/tfjs";
import * as handpose from "@tensorflow-models/handpose";
import Webcam from "react-webcam";
import "./App.css";
//import { drawHand } from "./utilities";

import {CountOne} from "./count_1"; 
import {CountTwo} from "./count_2"; 
import {CountThree} from "./count_3"; 
import {CountFour} from "./count_4"; 
import Geogebra from 'react-geogebra';

import * as fp from "fingerpose";

import logo from './logo.png'; // Tell webpack this JS file uses this image

function App() {
  const webcamRef = useRef(null);
  const canvasRef = useRef(null);

  const runHandpose = async () => {
    const net = await handpose.load();
    console.log("Handpose model loaded.");
    //  Loop and detect hands
    setInterval(() => {
      detect(net);
    }, 1000);
  };



  const detect = async (net) => {
    // Check data is available
    if (
      typeof webcamRef.current !== "undefined" &&
      webcamRef.current !== null &&
      webcamRef.current.video.readyState === 4
    ) {
      // Get Video Properties
      const video = webcamRef.current.video;
      const videoWidth = webcamRef.current.video.videoWidth;
      const videoHeight = webcamRef.current.video.videoHeight;

      // Set video width
      webcamRef.current.video.width = videoWidth;
      webcamRef.current.video.height = videoHeight;

      // Set canvas height and width
      canvasRef.current.width = videoWidth;
      canvasRef.current.height = videoHeight;

      // Make Detections
      const hand = await net.estimateHands(video);
      // console.log(hand);
      if (hand.length > 0) {
        const GE = new fp.GestureEstimator([
          CountOne,
          CountTwo,
          CountThree,
          CountFour,
          fp.Gestures.ThumbsUpGesture
        ]);
        const gesture = await GE.estimate(hand[0].landmarks, 7);
        if (gesture.gestures !== undefined && gesture.gestures.length > 0) {
          //console.log(gesture.gestures);

          const confidence = gesture.gestures.map(
            (prediction) => prediction.confidence
          );
          const maxConfidence = confidence.indexOf(
            Math.max.apply(null, confidence)
          );
          console.log(gesture.gestures[maxConfidence].name);
          //console.log(gesture.gestures[maxConfidence].name);
          if(gesture.gestures[maxConfidence].name==="count_1"){
            selectOne();
          }
          else if(gesture.gestures[maxConfidence].name==="count_2"){
            selectTwo();
          }
          else if(gesture.gestures[maxConfidence].name==="count_3"){
            selectThree();
          }
          else if(gesture.gestures[maxConfidence].name==="count_4"){
            selectFour();
          }
          else if(gesture.gestures[maxConfidence].name==="thumbs_up"){
            const app1 = window.appId;
            var q = app1.getValue('q');
            if(q==0){
              selectBegin();
            }
            if(q>0 && q<16){
              //console.log(q);
              selectSubmit();
              setTimeout(selectNext,4000);
            }
          }
        }
      }

      // Draw mesh
      //const ctx = canvasRef.current.getContext("2d");
      //drawHand(hand, ctx);
    }
  };
  function selectOne() {
    const app1 = window.appId;
    console.log(app1.evalCommand('RunClickScript(tickBox01)'))
  }

  function selectTwo() {
    const app1 = window.appId;
    console.log(app1.evalCommand('RunClickScript(tickBox02)'))
  }

  function selectThree() {
    const app1 = window.appId;
    console.log(app1.evalCommand('RunClickScript(tickBox03)'))
  }

  function selectFour() {
    const app1 = window.appId;
    console.log(app1.evalCommand('RunClickScript(tickBox04)'))
  }

  function selectBegin() {
    const app1 = window.appId;
    app1.evalCommand('RunClickScript(BeginButton)')
  }

  function selectSubmit() {
    const app1 = window.appId;
    app1.evalCommand('RunClickScript(submit)')
  }

  function selectNext() {
    const app1 = window.appId;
    app1.evalCommand('RunClickScript(NextButton)')
  }

  useEffect(()=>{
    runHandpose()
  },[]);
 
  return (
    <div className="App" style={{
      backgroundColor: "aliceblue"
    }}>
      <header className='navbar' style={{margin:0}}>
          <div className='navbar__title navbar__item'><img src={logo} alt="Logo" style={{width:"200px"}}/></div>
          
          <div className='navbar__item'>Gesture-Rise Quiz</div>
       
      </header>
      <div className="Saket">    
      <Geogebra
        id="appId"
        material_id="u5ty3njh"
        width="1200"
        height="650"
        enableUndoRedo="false"
        showToolBar= "false" 
        borderColor="null"
        showMenuBar="false"
        showAlgebraInput= "false"
      />
      </div>
      
        
      <header className="App-header">
        <Webcam
          ref={webcamRef}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 320,
            height: 240
          }}
        />
        <canvas
          ref={canvasRef}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            width: 440,
            height: 380,
          }}
        />
      </header>
    </div>
  );
}

export default App;
