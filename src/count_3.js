import {Finger, FingerCurl, FingerDirection, GestureDescription} from 'fingerpose'; 

// Define Gesture Description
export const CountThree = new GestureDescription('count_3'); 

// thumb:
// - not curled
// - vertical up (best) or diagonal up left / right
CountThree.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0);
CountThree.addDirection(Finger.Index, FingerDirection.VerticalUp, 0.8);
CountThree.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 0.2);
CountThree.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 0.2);

CountThree.addCurl(Finger.Middle, FingerCurl.NoCurl, 1.0);
CountThree.addDirection(Finger.Middle, FingerDirection.VerticalUp, 1);

CountThree.addCurl(Finger.Ring, FingerCurl.NoCurl, 1.0);
CountThree.addDirection(Finger.Ring, FingerDirection.VerticalUp, 1);
CountThree.addDirection(Finger.Ring, FingerDirection.DiagonalUpLeft, 0.2);
CountThree.addDirection(Finger.Ring, FingerDirection.DiagonalUpRight, 0.2);


CountThree.addCurl(Finger.Thumb, FingerCurl.HalfCurl, 0.5);
CountThree.addCurl(Finger.Thumb, FingerCurl.NoCurl, 0.5);
CountThree.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1);
CountThree.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1);

for(let finger of [Finger.Pinky]) {
    CountThree.addCurl(finger, FingerCurl.FullCurl, 1);
    CountThree.addDirection(finger, FingerDirection.VerticalDownLeft, 0.8);
    CountThree.addDirection(finger, FingerDirection.VerticalDownRight, 0.8);
}
  
CountThree.setWeight(Finger.Middle, 2);
CountThree.setWeight(Finger.Ring, 2);
  
  

