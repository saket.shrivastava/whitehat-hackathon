import {Finger, FingerCurl, FingerDirection, GestureDescription} from 'fingerpose'; 

// Define Gesture Description
export const CountFour = new GestureDescription('count_4'); 

// thumb:
// - not curled
// - vertical up (best) or diagonal up left / right
CountFour.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0);
CountFour.addDirection(Finger.Index, FingerDirection.VerticalUp, 0.8);
CountFour.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 0.2);
CountFour.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 0.2);

CountFour.addCurl(Finger.Middle, FingerCurl.NoCurl, 1.0);
CountFour.addDirection(Finger.Middle, FingerDirection.VerticalUp, 1);
CountFour.addDirection(Finger.Middle, FingerDirection.DiagonalUpLeft, 0.2);
CountFour.addDirection(Finger.Middle, FingerDirection.DiagonalUpRight, 0.2);

CountFour.addCurl(Finger.Ring, FingerCurl.NoCurl, 1.0);
CountFour.addDirection(Finger.Ring, FingerDirection.VerticalUp, 1);
CountFour.addDirection(Finger.Ring, FingerDirection.DiagonalUpLeft, 0.2);
CountFour.addDirection(Finger.Ring, FingerDirection.DiagonalUpRight, 0.2);

CountFour.addCurl(Finger.Pinky, FingerCurl.NoCurl, 1.0);
CountFour.addDirection(Finger.Pinky, FingerDirection.VerticalUp, 1);
CountFour.addDirection(Finger.Pinky, FingerDirection.DiagonalUpLeft, 0.2);
CountFour.addDirection(Finger.Pinky, FingerDirection.DiagonalUpRight, 0.2);

CountFour.addCurl(Finger.Thumb, FingerCurl.HalfCurl, 0.5);
CountFour.addCurl(Finger.Thumb, FingerCurl.NoCurl, 0.5);
CountFour.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1);
CountFour.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1);

CountFour.setWeight(Finger.Middle, 2);
CountFour.setWeight(Finger.Ring, 2);
CountFour.setWeight(Finger.Pinky, 2);
