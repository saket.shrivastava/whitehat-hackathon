import {Finger, FingerCurl, FingerDirection, GestureDescription} from 'fingerpose'; 

// Define Gesture Description
export const CountOne = new GestureDescription('count_1'); 

// thumb:
// - not curled
// - vertical up (best) or diagonal up left / right
CountOne.addCurl(Finger.Index, FingerCurl.NoCurl, 1.0);
CountOne.addDirection(Finger.Index, FingerDirection.VerticalUp, 1.0);
CountOne.addDirection(Finger.Index, FingerDirection.DiagonalUpLeft, 0.8);
CountOne.addDirection(Finger.Index, FingerDirection.DiagonalUpRight, 0.8);

CountOne.addCurl(Finger.Thumb, FingerCurl.HalfCurl, 0.8);
CountOne.addCurl(Finger.Thumb, FingerCurl.NoCurl, 0.8);
CountOne.addDirection(Finger.Thumb, FingerDirection.DiagonalUpLeft, 1);
CountOne.addDirection(Finger.Thumb, FingerDirection.DiagonalUpRight, 1);

for(let finger of [Finger.Middle, Finger.Ring, Finger.Pinky]) {
    CountOne.addCurl(finger, FingerCurl.FullCurl, 1);
    CountOne.addCurl(finger, FingerCurl.HalfCurl, 1);
    CountOne.addDirection(finger, FingerDirection.VerticalDown, 1);
}
  
CountOne.setWeight(Finger.Index, 2);
CountOne.setWeight(Finger.Middle, 2);


