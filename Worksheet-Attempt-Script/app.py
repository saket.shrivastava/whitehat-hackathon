from PIL import Image, ImageOps
im1 = Image.open("option1.png")
im = ImageOps.grayscale(im1)

full_img = Image.open("worksheet-unsolved.jpg")
#solved worksheet as the input
solved_img_uncropped = Image.open("solved.png")
solved_img_uncropped_grey = ImageOps.grayscale(solved_img_uncropped)

solved_img = solved_img_uncropped_grey.crop((150, 0, 1425, 9900))
#solved_img.show()
#print(full_img)

total_question = 9
attempted_count = 0

def isQuestionAttempted(img):
    pixels = img.getdata()          # get the pixels as a flattened sequence
    black_thresh = 40
    nblack = 0
    for pixel in pixels:
        if pixel < black_thresh :
            nblack += 1
    n = len(pixels)    
    if (nblack) > 0:
        return 1
    return 0

attempted_count+=isQuestionAttempted(solved_img.crop((60, 300, 1170, 870)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 871, 1170, 1572)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 1900, 1170, 2447)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 2447, 1170, 3120)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 3570, 1170, 4211)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 4211, 1170, 4800)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 5200, 1170, 5830)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 5830, 1170, 6500)))
attempted_count+=isQuestionAttempted(solved_img.crop((60, 6840, 1170, 9700)))


print( "PERCENT ATTEMPTED -- ",(attempted_count/total_question)*100 )